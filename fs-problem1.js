const fs = require('fs');
const path = require('path');

function createdirectory(directoryPath, callback) {
    fs.mkdir(directoryPath, (err) => {
        if (err) {
            if (err.code === 'EEXIST') {
                console.log(`Directory ${directoryPath} already exists`);
                callback(null);
            }
            else {
                callback(err);
            }
        }
        else {
            console.log(`Directory ${directoryPath}  is created Successfully `);
            callback(null);
        }
    })
}

function genearateRandomdata() {

    return {
        id: Math.floor(Math.random() * 1000),
        name: `Item-${Math.floor(Math.random() * 100)}`,
        value: Math.random() * 100
    };
}

function addRandomdatatoJSonfiles(directoryPath, numberofFiles, callback) {
    let filePaths = [];
    function adddatatofile(i) {
        if (i > numberofFiles) {
            callback(null, filePaths);
            return;
        }
        const filename = `file${i}.json`;
        const filepath = path.join(directoryPath, filename);
        const jsondata = genearateRandomdata();

        const jsonstring = JSON.stringify(jsondata, null, 2);

        fs.writeFile(filepath, jsonstring, (err) => {
            if (err) {
                callback(err);
                return;
            }
            console.log(`Data added to ${filename}`);
            filePaths.push(filepath);
            adddatatofile(i + 1);

        })
    }
    adddatatofile(1);
}

function deletefiles(filepaths, callback) {
    function deletefile(i) {
        if (i >= filepaths.length) {
            callback(null);
            return;
        }

        fs.unlink(filepaths[i], (err) => {
            if (err) {
                callback(err);
                return;
            }
            console.log(`File ${filepaths[i]} Deleted Successfully`);
            deletefile(i + 1);
        });
    }
    deletefile(0);

}

module.exports = {
    createdirectory,
    addRandomdatatoJSonfiles,
    deletefiles
};

