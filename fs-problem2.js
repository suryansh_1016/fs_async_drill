const fs = require("fs");

function writeFileName(filename) {
    fs.appendFile("filenames.txt", filename + "\n", (err) => {
        if (err) {
            console.log("error occured: ", err);
        } else {
            console.log(filename + " is written");
        }
    });
}

function Path(filePath) {
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.log("error occured while reading file : ", err);
        }
        else {
            console.log("reading successful");
            let UpperCase = data.toUpperCase();
            const newFileName = 'UpperCase_lipsum_1.txt';
            writeFile(newFileName, UpperCase);

        }
    })
}

function writeFile(newFileName, Uppercase) {
    fs.writeFile(newFileName, Uppercase, (err) => {
        if (err) {
            console.log("error occured while writing : ", err);
        }
        else {
            console.log("write successful");

            fs.appendFile('filenames.txt', newFileName + "\n", (err) => {
                if (err) {
                    console.log(" error appening new file's name to filesnames.txt", err)
                }
                else {
                    console.log(`Filename ${newFileName} has been appended to filenames.txt`);
                    lowerCaseAndSplit(Uppercase, sort);
                }
            });
        }
    });
}


function lowerCaseAndSplit(content, sort) {
    let lower = content.toLowerCase();
    let fileName = "lowerCase_lipsum.txt";
    let sentences = lower.split(". ");
    sentences.forEach((sentence) => {
        function readAndDeleteFiles() {
            fs.readFile("filenames.txt", "utf8", (err, data) => {
                if (err) {
                    console.log("error occured: ", err);
                } else {
                    console.log(data);
                    let filename = data.split("\n");
                    for (let index = 0; index < filename.length - 1; index++) {
                        fs.unlink(filename[index], (err) => {
                            if (err) {
                                console.log("error occured in deletion: ", err);
                            }
                        })
                    }
                }
            });
        }
        fs.appendFile(fileName, sentence + "\n", (err) => {
            if (err) {
                console.log("error occured while appending file : ", err)
            }
        });
    });
    writeFileName(fileName);
    console.log("converted to lowercase");
    sort(lower, readAndDelete);
}

function sort(content, readAndDelete) {
    let contentArray = content.split(" ");
    contentArray.sort();
    let fileName = "sortedContent.txt";
    contentArray.forEach((index) => {
        fs.appendFile(fileName, index + "\n", (err) => {
            if (err) {
                console.log("error occured: ", err);
            }
        });
    });
    writeFileName(fileName);
    console.log("write successful");
    readAndDelete();
}


function readAndDelete() {
    fs.readFile("filenames.txt", "utf8", (err, data) => {
        if (err) {
            console.log("error occured: ", err);
        } else {
            console.log(data);
            let filename = data.split("\n");
            for (let index = 0; index < filename.length - 1; index++) {
                fs.unlink(filename[index], (err) => {
                    if (err) {
                        console.log("error occured in deletion: ", err);
                    }
                })
            }
        }
    });
}



module.exports = Path;

